#!/bin/bash

# Instalação das dependências necessárias
apt-get update
apt-get install -y python3

# Criação do arquivo Python
cat > server.py <<EOF
import socket

HOST, PORT = '', 8005

listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listen_socket.bind((HOST, PORT))
listen_socket.listen(1)

print(f'Serving HTTP on port {PORT}...')

while True:
    client_connection, client_address = listen_socket.accept()
    request = client_connection.recv(1024)
    print(request.decode('utf-8'))

    http_response = b'HTTP/1.1 200 OK\n\nHello world!!! Teste Deploy em VM com Spinnaker\n'
    client_connection.sendall(http_response)
    client_connection.close()
EOF

# Execução do servidor 127.0.0.1:8005
python3 server.py
