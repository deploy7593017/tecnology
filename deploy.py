from decouple import config
import requests
import subprocess

tecnologia = config('tecnologia')
siglamodulo = config('siglamodulo')
componente = config('componente')

user = config('user')
passw = config('passw')


#1 - Decouple reconhece tecnologia no arquivo .env
def deploy_tecnologia_sh():
    # URL do arquivo raw 
    url = f'https://{user}:{passw}@gitlab.com/deploy7593017/tecnology/-/raw/main/{tecnologia}.sh'
    # Baixa o conteúdo do arquivo
    response = requests.get(url)
    content = response.content.decode()

    # Cria o arquivo run.sh a partir do script da tecnologia
    with open('run.sh', 'w') as f:
        f.write(content)

    # Torna o arquivo executável
    subprocess.run(['chmod', '+x', 'run.sh'])

    # Executa o arquivo
    subprocess.run(['./run.sh']) 


#4 - Executar script tecnologia.sh
if __name__ == '__main__':
    deploy_tecnologia_sh()
